<html>
<head>
	<link href="style.css" rel="stylesheet">

	<title>Conway Game</title>
	<script src="js/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="title">John Conway's Game of life</div>
		<div class="instructions">
			<h2>Instructions</h2>
			<p>Every black cell is considered dead, you can select as manny cells as you want so it comes back to life, once you start the game, every cell with less than 2 or more than 3 alive cells as neighbor dies, and every dead cell with exacly 3 alive neighbors comes back to life</p>
			<p>1. Press on each cell you want to come back to life</p>
			<p>2. Press Play to start</p>
			<p>3. Press Restart to start all over again</p>
		</div>
		<div class="board">
			<table id="game">
				<?php 
				for ($i = 0; $i < 10; $i++) { 
					echo "<tr id='tr-".$i."' class='tr'>";
					for ($e = 0; $e < 10; $e++) {
						echo "<td id='".$i."-".$e."' class='td' onclick=toggleCellsStatus('".$i."-".$e."')></td>";
					}
					echo"</tr>";
				}
				?>
			</table>
		</div>
		<div class="story">
			<h2>Story of the Game</h2>
			<p>
			The game is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves, or, for advanced players, by creating patterns with particular properties.
			</p>
		</div>
		<div class="buttonContainer">
			<div id="play" class="button">Play</div>
			<div class="button" value="Refresh Page" onClick="window.location.reload()">Restart</div>
		</div>
		
	</div>
	
</body>
<script type="text/javascript">
var customCells = [];

function toggleCellsStatus(cellId){
	var cell= document.getElementById(cellId);
	var id = cellId.split("-");
	if($(cell).hasClass('active')==true) {
		$(cell).removeClass('active');
		customCells.pop([id[0],id[1]]);
	} else {
		$(cell).addClass('active');
		customCells.push([id[0],id[1]]);
	}
}

	//pajaro [[0,0],[2,0],[2,1],[1,2],[2,2]];
	//tricaja [[2,2],[2,3],[2,4]];
	//cuadrado [[2,2],[2,3],[3,2],[3,3]];

var first = 'true';
var active = customCells;

var memory = active;
	function calculateBoard(){
		
    	$.post('games.php', {
    			values: first,
    			list: memory,
    		},function(data,status){
    			first = 'false';
    			memory = data["value"];
    			$(game).html(data["html"]);
    		}, "json");
	}
	
	var play = document.getElementById("play");
	var game = document.getElementById("game");
	var condition = true;
	$(document).ready(function() {
    	$(play).click(function() {

    		if(active == '') {
    			alert("Please select any cell");
    		} else {
    			if(condition){
    				condition=false;
    				calculateBoard();
    				var inter = setInterval(calculateBoard, 1000);
    			} 			
    		}
		}); 
	});
</script>
</html>
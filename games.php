<?php 


error_reporting( E_ERROR );
function games($data, $html){
	$i = 0;
	foreach ($data as $a => $b) {
		$html .= "<tr id='tr-".$i."' class='tr'>";
		$i++;
		$e = 0;
		foreach ($b as $c => $d) {
			if($d == 'alive'){
				$html .= "<td id='td-".$e."' class='td active'></td>";	
			} else {
				$html .= "<td id='td-".$e."' class='td'></td>";
			}
			$e++;
		}
		$html .= "</tr>";
	}
	$values = ["value" => $data, "html" => $html];

	echo json_encode($values);
}


if($_POST['values'] == 'true') {
	for ($i = 0; $i < 10; $i++) {
		for ($e = 0; $e < 10; $e++) { 
			$data[$i][$e] = "dead";
		}
	}
	foreach ($_POST['list'] as $key => $value) {
		$data[$value[0]][$value[1]] = "alive";
	}

	$board = games($data, $html = '');
} else {
	$data = $_POST['list'];
	foreach ($data as $y => $value) {
		foreach ($value as $x => $val) {
			$aliveNeighborCounter = 0;
			
			$sidestatus=[
				$data[$y + 1][$x + 1],
				$data[$y - 1][$x - 1],
				$data[$y - 1][$x],
				$data[$y - 1][$x + 1],
				$data[$y][$x - 1],
				$data[$y][$x + 1],
				$data[$y + 1][$x - 1],
				$data[$y + 1][$x]
			];
			
			foreach ($sidestatus as $k => $v) {
				if($v == 'alive'){
					$aliveNeighborCounter = $aliveNeighborCounter + 1;
				}
			}
			//rules
			$status[$y][$x] = $data[$y][$x];
			
			if($status[$y][$x] == 'alive') {
				if($aliveNeighborCounter <= 1 || $aliveNeighborCounter >= 4){
					$status[$y][$x] = 'dead';
				}
			} elseif($status[$y][$x] == 'dead') {
				if($aliveNeighborCounter == 3){
					$status[$y][$x] = 'alive';
				}
			}
		}
	}
	$board = games($status, $html = '');
}
?>